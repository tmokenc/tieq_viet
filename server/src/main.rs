#[macro_use]
extern crate actix_web;

use actix_web::{App, HttpResponse, HttpServer};
use std::env;
use std::process::{Command, Stdio};
use tokio::io::AsyncReadExt;

const STATIC_PATH: &str = "./tieq_viet/pkg";
const INDEX_FILE: &str = "./main.html";

#[get("/")]
async fn index() -> Result<HttpResponse, actix_web::Error> {
    let mut buffer = String::new();

    tokio::fs::File::open(INDEX_FILE)
        .await?
        .read_to_string(&mut buffer)
        .await?;

    Ok(HttpResponse::Ok().body(buffer))
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();

    println!("Building wasm module");
    build_wasm_file()?; // nothing has been awaiting yet so blocking IO here is fine

    let ip = env::var("IP").unwrap_or(String::from("127.0.0.1:8080"));
    println!("The server is up on {}", &ip);

    HttpServer::new(move || {
        App::new()
            .service(index)
            .service(actix_files::Files::new("/tieq_viet/pkg/", STATIC_PATH))
    })
    .bind(ip)?
    .run()
    .await
}

fn build_wasm_file() -> std::io::Result<()> {
    let mut dir = env::current_dir()?;

    if dir.ends_with("server") {
        dir.pop();
    }

    Command::new("wasm-pack")
        .args(&["build", "tieq_viet", "--target", "web"])
        .current_dir(dir)
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .spawn()?
        .wait()?;

    Ok(())
}
